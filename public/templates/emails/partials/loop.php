<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

//Loop settings
$loop_products_no = $email_wp_query->post_count ;
$products_per_row = 2;
$loop_counter = 1;

?>

<?php if ( $email_wp_query->have_posts() ) : while ( $email_wp_query->have_posts() ) : $email_wp_query->the_post(); ?>

	<?php if ( $loop_counter % $products_per_row == 1 ) { include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/row-start.php'; } ?>

		<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop-column-start.php'; ?>
		<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop-product.php'; ?>
		<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop-column-end.php'; ?>
		
		<?php 
		//When product no is odd, add an empty product placeholder, to fix alignment.
		if ( $email_wp_query->post_count  % 2 != 0 && ( $loop_products_no - $loop_counter == 0 ) ){
			
			include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop-column-start.php';
			include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop-product-empty.php';
			include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop-column-end.php';
			$loop_counter ++ ;
		}
		?>

	<?php if ( $loop_counter % $products_per_row == 0 ) { include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/row-end.php'; } ?>

	<?php $loop_counter ++ ; ?>

<?php endwhile; endif;?>

<?php wp_reset_postdata(); ?>