<?php 
global $product;

$single_product_html = file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/single-product.html');

/*
 * Crowdfunding expiry countdown
*/

$single_product_countdown_html ='';

$crowdfunding_expiry_timestamp = get_post_meta( get_the_ID(), '_tps_crowdfunding_expiry', true ) ;

//Proceed only if the product has a crwodfunding timestamp , which higher than current time
if ( !empty ( $crowdfunding_expiry_timestamp ) && ( time() < $crowdfunding_expiry_timestamp )  )
{
	$timezone_string = get_option( 'timezone_string' , 'Europe/Athens' );

	$crowdfunding_expiry = new DateTime();

	$crowdfunding_expiry->setTimestamp( $crowdfunding_expiry_timestamp );

	$crowdfunding_expiry->setTimezone( new DateTimeZone( $timezone_string ) );

	$crowdfunding_expiry_date = $crowdfunding_expiry -> format('Y/m/d H:i') ;
	
	$single_product_countdown_html = vsprintf( 
		file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/countdown.html') ,  
		array( TPS_NEWSLETTER_COUNTDOWN_URL . $crowdfunding_expiry_date )
	);
}

/*
1: product_image_link,
2: product_image_url,
3: product_image_alt,
4: product_name,
5: product_promo_text
6: product_cta_button_link
7: product_cta_button_text
8: product_crowdfunding_countdown
*/

$single_product_args = array(
	
	tps_utm ( get_the_permalink() , ['utm_content' => 'product_image','utm_term' => sanitize_text_field ( get_the_title() ) ] ), // 1: product_image_link
	get_the_post_thumbnail_url(), // 2: product_image_url
	get_the_title(), // 3: product_image_alt
	get_the_title(), // 4: product_name
	sanitize_text_field ( get_the_excerpt() ), // 5: product_promo_text
	tps_utm ( get_the_permalink() , ['utm_content' => 'product_cta_button','utm_term' => sanitize_text_field ( get_the_title() ) ] ), // 6 product_cta_button_link
	__('LEARN MORE' , 'tps-newsletter'), // 7: product_cta_button_text
	$single_product_countdown_html // 8: product_crowdfunding_countdown

);

//echo $single_product_countdown_html;


echo vsprintf( $single_product_html , $single_product_args );

?>

