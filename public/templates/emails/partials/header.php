<?php 

$header_text = file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/header.html');

/*
1:home_url,
2:logo_url,
3:logo_alt,
4:webversion_text,
5:webversion_link_url,
6: webversion_link_text
*/

global $wp;

$header_args = array (
	
	tps_utm ( get_home_url() , [ 'utm_content' => 'logo' ] ), //1:home_url
	'http://assets.theparentsshop.com/emails/logo/header.png', //2:logo_url
	'The Parents Shop', //3:logo_alt
	__('Images not showing?' , 'tps-newsletter'), //4:webversion_text
	home_url( $wp->request ), //5:webversion_link_url
	__('Click here for the web version' , 'tps-newsletter') //6: webversion_link_text

	);

echo vsprintf( $header_text , $header_args );

?>