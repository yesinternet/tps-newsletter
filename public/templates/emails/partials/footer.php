<?php 

$footer_text = file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/footer.html');

/*
footer.html 
1: social_heading
2: facebook_url
3: twitter_url
4: googleplus_url
5: youtube_url
6: instagram_url
7: pinterest_url
8: company_name
9: company_address
10: unsubscribe_text
11: unsubscribe_link_text
*/
	
$footer_args = array (
	
	__('Connect with us' , 'tps-newsletter'), // 1: social_heading
	TPS_FACEBOOK_URL, // 2: facebook_url
	TPS_TWITTER_URL, // 3: twitter_url
	TPS_GOOGLE_PLUS_URL, // 4: google_plus_url
	TPS_YOUTUBE_URL, // 5: youtube_url
	TPS_INSTAGRAM_URL, // 6: instagram_url
	TPS_PINTEREST_URL, // 7: pinterest_url
	__('The Parents Shop (by Milatu Ltd)' , 'tps-newsletter'), // 8: company_name
	__('Spyrou Kyprianou 41-43, Megaro Patroklou, 7104, Larnaka, Cyprus' , 'tps-newsletter'), // 9: company_address
	__('You received this email because you signed up to receive updates from us.' , 'tps-newsletter'), // 10: unsubscribe_text
	__('Unsubscribe' , 'tps-newsletter') // 11: unsubscribe_link_text

	);

echo vsprintf( $footer_text , $footer_args );

?>