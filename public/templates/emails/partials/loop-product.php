<?php 

$loop_product_text = file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/loop-product.html');

/*
loop-product.html 
1: image_href,
2: image_url,
3: name,
4: promo_text,
5: button_href,
6: button_text
*/

$loop_product_text_args = array (
	
	tps_utm ( get_permalink() , ['utm_content' => 'product_image','utm_term' => sanitize_text_field ( get_the_title() ) ] ) ,
	get_the_post_thumbnail_url( null , 'shop_catalog'),
	mb_strimwidth( sanitize_text_field ( get_the_title() ) , 0, 45, "...") ,
	mb_strimwidth( sanitize_text_field ( sanitize_text_field ( get_the_excerpt() ) ) , 0, 92, "..."),
	tps_utm ( get_permalink() , ['utm_content' => 'read_more','utm_term' => sanitize_text_field ( get_the_title() ) ] ),
	__('Read more' , 'tps-newsletter')

	);

echo vsprintf( $loop_product_text , $loop_product_text_args );

?>