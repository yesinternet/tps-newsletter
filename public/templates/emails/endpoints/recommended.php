<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php 

	$meta_query   = WC()->query->get_meta_query();
	$tax_query   = WC()->query->get_tax_query();
	$tax_query[] = array(
		'taxonomy' => 'product_visibility',
		'field'    => 'name',
		'terms'    => 'featured',
		'operator' => 'IN',
	);

	$query_args = array(
		'posts_per_page' => TPS_NEWSLETTER_EMAILS_LOOP_PRODUCTS_NO,
		'orderby'        => 'date',
		'order'          => 'desc',
		'no_found_rows'  => 1,
		'post_status'    => 'publish',
		'post_type'      => 'product',
		'meta_query'     => $meta_query,
		'tax_query'      => $tax_query,
	);

	$email_wp_query = new WP_Query( $query_args );

?>

<?php do_action ('tps-newletter-email-html-start'); ?>

<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop.php'; ?>

<?php do_action ('tps-newletter-email-html-end'); ?>