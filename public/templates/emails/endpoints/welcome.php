<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php do_action ('tps-newletter-email-html-start'); ?>

<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/row-start.php'; ?>

<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/column-start.php'; ?>

<?php 

$welcome_html = file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/welcome.html');

/*
1: home_url,
2: welcome_image_url,
3: welcome_image_alt,
4: welcome_heading,
5: welcome_text,
6: welcome_cta_button_link
7: welcome_cta_button_text
*/

global $wp;

$welcome_args = array (
	
	tps_utm ( get_home_url() , [ 'utm_content' => 'welcome_image' ] ), // 1: home_url
	'http://assets.theparentsshop.com/emails/welcome/happy-family.jpg', // 2: welcome_image_url
	__('WELCOME' , 'tps-newsletter'), // 3: welcome_image_alt
	__('SO NICE TO MEET YOU' , 'tps-newsletter'), // 4: welcome_heading
	__('<p style="text-align:center;">We couldn’t be more excited to have you with us.</p><p>The Parents Shop is a non stop discovering product place created by inspired parents. Our goal is to help you discover the products of your dreams through an intensive research based on our global network. Be the first to know and stay tuned for daily new innovative products that make parenting an easier job to do.</p><p>From now on your inbox is about to be a lot more inspiring!</p><p style="text-align:center;">In the meantime, check out these recommended products.</p>' , 'tps-newsletter'), // 5: welcome_text
	tps_utm ( get_home_url() , [ 'utm_content' => 'welcome_cta_button' ] ), // 6: welcome_cta_button_link
	__('CHECK OUT NOW' , 'tps-newsletter') // 7: welcome_cta_button_text

);

echo vsprintf( $welcome_html , $welcome_args );

?>

<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/column-end.php'; ?>

<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/row-end.php'; ?>

<?php do_action ('tps-newletter-email-html-end'); ?>