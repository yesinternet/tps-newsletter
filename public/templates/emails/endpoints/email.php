<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php do_action ('tps-newletter-email-html-start'); ?>


<?php 

global $wp_query ;

$email_wp_query = $wp_query	;

?>

<?php //Single product Loop ------------------------------------------------------------------------- ?>

<?php if ( $email_wp_query->post_count == 1 ) : ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/row-start.php'; ?>

		<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/column-start.php'; ?>

		<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/single-product.php'; ?>

		<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/column-end.php'; ?>

		<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/row-end.php'; ?>
		
	<?php endwhile; endif;?>

<?php else:?>

<?php include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop.php'; ?>

<?php endif;?>



<?php do_action ('tps-newletter-email-html-end'); ?>