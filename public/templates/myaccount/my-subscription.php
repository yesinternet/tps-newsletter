<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$current_user = wp_get_current_user();

$email = $current_user->user_email ;

$contact = Tps_Newsletter_EE_API::contact_load( $email );

if ( $contact ['success'] === true ){

	$status_map = Tps_Newsletter_EE_API::contact_status_map() ;
	$status = $status_map [ $contact ['data'] -> status ] ;
}

/**
 * The template for displaying the WC My Account newsletter endpoint
 *
 */
?>

<?php
	/**
	 * tps_newsletter_woocommerce_account_newsletter_content_start hook.
	 *
	 */
	 do_action( 'tps_newsletter_woocommerce_account_newsletter_content_start' );

?>

<?php if ( $contact ['success'] === false ) :?>

	<?php include TPS_NEWSLETTER_TEMPLATES_PATH . '/contact-not-found-notice.php'; ?>
	<?php return;?>
<?php endif;?>

<?php if ( $contact ['data'] -> status <= 0 ) :?>
	
	<div class="alert alert-success">
		<p><?php _e('Your current email subscription status is Active' , 'tps-newsletter');?></p>
	</div>
	
	<p><i><small><?php _e('If you need to unsubscribe, please click on the unsubscribe link located at the bottom of the emails that you have received.' , 'tps-newsletter');?></small></i></p>

<?php endif;?>

<?php if ( $contact ['data'] -> status > 0 ) :?>
	
	<?php include TPS_NEWSLETTER_TEMPLATES_PATH . '/contact-not-found-notice.php'; ?>

<?php endif;?>



<?php
	/**
	 * tps_newsletter_woocommerce_account_newsletter_content_end hook.
	 *
	 */
	 do_action( 'tps_newsletter_woocommerce_account_newsletter_content_end' );

?>