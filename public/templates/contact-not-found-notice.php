<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="alert alert-danger">
	<p class="text-center"><?php _e( 'It seems that you are not subscribed to our mailing list.' , 'tps-newsletter' );?></p> 
</div>
		
<p class="text-center">
	<?php _e( 'Subscribe to our mailing list and be the first to know about new, innovative products for parents, right in your inbox!' , 'tps-newsletter' );?>
</p>	
	
<p class="text-center"><button  type="button" class="btn btn-primary tps-newsletter-subscribe" data-loading-text="<?php _e( 'Subscribing...' , 'tps-newsletter' );?>" autocomplete="off"><?php _e( 'Subscribe now' , 'tps-newsletter' );?></button> </p>
		
<p class="tps-newsletter-subscribe-error-wrapper text-danger text-center"></p>