<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://theparentsshop.com/
 * @since      1.0.0
 *
 * @package    Tps_Newsletter
 * @subpackage Tps_Newsletter/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Tps_Newsletter
 * @subpackage Tps_Newsletter/public
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Newsletter_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tps_Newsletter_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tps_Newsletter_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tps-newsletter-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tps_Newsletter_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tps_Newsletter_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tps-newsletter-public.js', array( 'jquery' ), $this->version, true );
		wp_localize_script( $this->plugin_name, 'tps_newsletter_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}

	/**
	 * Add newsletter menu item to Woocommerce account menu items.
	 *
	 * @since    1.0.0
	 */
	public function add_woocommerce_newsletter_account_menu_item( $items ) {

		//Unset logout item in order to reset it to the end of the array.
		$logout_item = $items['customer-logout'];
		
		unset($items['customer-logout']);
		
		$items[TPS_NEWSLETTER_MY_ACCOUNT_ENDPOINT]= __('Email subscription' , 'tps-newsletter' );
		
		$items['customer-logout']= $logout_item;

		return $items;

	}


	

}
