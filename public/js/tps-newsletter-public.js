(function( $ ) {
	'use strict';

	 $(function () {

         $( "body" ).on('click', '.tps-newsletter-subscribe', function(event){

            event.preventDefault();
            
            var button = $(this);
            var button_error = $('.tps-newsletter-subscribe-error-wrapper');
            
            button.button('loading');
            
            $.post(
                tps_newsletter_ajax_object.ajax_url,
                {
                    'action': 'tps_newsletter_subscribe',
                },
                function ( response ) {
					
                    if( !response || response == '0' ){
                    	button_error.text('An error occured while trying to add contact data. Please try again.');
                        button.button('reset');
                        return;
                    }

                    if ( !response.success ){
                    	button_error.text(response.message);
                        button.button('reset');
                        return;
                    }

                    location.reload();

                });
        });

    });

})( jQuery );
