<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Tps_Newsletter_EE_API class
 */
class Tps_Newsletter_EE_API {	

	/**
	 * Add contact to EE contacts
	 */
	public static function contact_add( $email , $contact_data ){

		$default_add_contact_data = array(
			'publicAccountID' => TPS_NEWSLETTER_ELASTICEMAIL_PUBLIC_ACCOUNT_ID, 
			'email' => $email,
			'source' => ApiTypes\ContactSource::ContactApi,
			'sendActivation' => false,
		);

		$add_contact_data = array_merge( $default_add_contact_data , $contact_data ); 

		try{

			$add_contact = \ElasticEmailClient\ApiClient::Request( 'contact/add', $add_contact_data );

	    	return array( 'success' => true , 'data' => $add_contact , 'message' => __('Contact added' , 'tps-newsletter') );
		}
		catch(Exception $e){

			error_log( $e -> __toString() );

			return array( 'success' => false , 'data' => $e -> __toString() , 'message' => __('An error occured while trying to add contact data. Please try again.' , 'tps-newsletter') );
		}
	}

	/**
	 * Delete contact from EE by user_id (permanent delete)
	 */
	public static function contact_delete( $email ){

		$delete_contact_data = array(
            'rule' => null,
            'emails' => $email,
            'allContacts' => false

		);

		try{
			
			\ElasticEmailClient\ApiClient::SetApiKey( TPS_NEWSLETTER_ELASTICEMAIL_API_KEY );
			
			$delete_contact = \ElasticEmailClient\ApiClient::Request('contact/delete', $delete_contact_data );

	    	return array( 'success' => true , 'data' => $delete_contact , 'message' => __('Contact has been deleted from the mailing list.' , 'tps-newsletter') );
		}
		catch(Exception $e){

			error_log( $e -> __toString() );

			return array( 'success' => false , 'data' => $e -> __toString() , 'message' => __('Error - Due to an error, the contact could not be deleted from the mailing list. Please try again.' , 'tps-newsletter') );
		}

	}

	public static function contact_update( $email , $contact_data = array() , $context = null ) {

		$default_contact_data = array(
			'email' => $email, 
			'clearRestOfFields' => false
		);

		$update_contact_data = array_merge( $default_contact_data , $contact_data ); 

		try{

			\ElasticEmailClient\ApiClient::SetApiKey( TPS_NEWSLETTER_ELASTICEMAIL_API_KEY );

			$contact_update = \ElasticEmailClient\ApiClient::Request( 'contact/update', $update_contact_data );

	    	return array( 'success' => true , 'data' => $contact_update , 'message' => __('Contact updated.' , 'tps-newsletter') );
		}
		catch(Exception $e){
			
			error_log( $e -> __toString() );

			return array( 'success' => false , 'data' => $e -> __toString() , 'message' => __('A request error eccured while trying to update the mailing list data. Please try again.' , 'tps-newsletter') );

		}

	}

	/**
	 * Change status
	 */
	public static function contact_changestatus( $email, $status ) {

		$change_status_data = array(
            'status' => $status,
            'rule' => null,
            'emails' => $email,
            'allContacts' => false
        );

		try{

			\ElasticEmailClient\ApiClient::SetApiKey( TPS_NEWSLETTER_ELASTICEMAIL_API_KEY );

			$change_status = \ElasticEmailClient\ApiClient::Request( 'contact/changestatus', $change_status_data );

	    	return array( 'success' => true , 'data' => $change_status , 'message' => __('Contact status changed' , 'tps-newsletter') );
		}
		catch(Exception $e){

			error_log( $e -> __toString() );

			return array( 'success' => false , 'data' => $e -> __toString() , 'message' => __('Contact status not changed' , 'tps-newsletter') );

		}

	}

	/**
	 * Update contact
	 *
	 */
	public static function contact_load( $email ) {

		try{

			\ElasticEmailClient\ApiClient::SetApiKey( TPS_NEWSLETTER_ELASTICEMAIL_API_KEY );

			$load_contact = \ElasticEmailClient\ApiClient::Request( 'contact/loadcontact' , array( 'email' => $email ));

	    	return array( 'success' => true , 'data' => $load_contact , 'message' => __('Contact loaded' , 'tps-newsletter') );

		}
		catch(Exception $e){

			$user = get_user_by( 'email', $email );

			error_log( $e -> __toString() );

			if( $e -> getMessage() == 'Could not find contact.' ){
				update_user_meta( $user -> ID , '_tps_newsletter_not_found' , true );
			}

			return array( 'success' => false , 'data' => null , 'message' => $e -> getMessage() );

		}

	}

	public static function contact_status_map() {

		$status_map = array(

			'-2' => 'Transactional',
			'-1' => 'Engaged',
			'0' => 'Active',
			'1' => 'Bounced',
			'2' => 'Unsubscribed',
			'3' => 'Abuse: mails sent to user without their consent',
			'4' => 'Inactive',
		);

		return $status_map;
	}

	public static function contact_source_map() {

		$source_map = array(

			'0' => '<strong>DeliveryApi</strong> - Source of the contact is from sending an email via our SMTP or HTTP API\'s',
			'1' => '<strong>ManualInput</strong> - Contact was manually entered from the interface.',
			'2' => '<strong>FileUpload</strong> - Contact was uploaded via a file such as CSV.',
			'3' => '<strong>WebForm</strong> - Contact was added from a public web form.',
			'4' => '<strong>ContactApi</strong> - Contact was added from the contact api.',
		);

		return $source_map;
	}

	public static function get_formatted_timestamp ( $timestamp = null ){

		$timezone_string = get_option( 'timezone_string' , 'Europe/Athens' );
		
		$datetime = new DateTime();
		$datetime->setTimestamp( $timestamp );
		$datetime->setTimezone( new DateTimeZone( $timezone_string ) );
		$date = $datetime -> format('Y-m-d');
		$time = $datetime -> format('H:i:s');

		//Return format as required by EE (YYYY-MM-DDTHH:MM:SS)

		return $date . 'T' . $time;
	}

}