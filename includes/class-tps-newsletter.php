<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://theparentsshop.com/
 * @since      1.0.0
 *
 * @package    Tps_Newsletter
 * @subpackage Tps_Newsletter/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Tps_Newsletter
 * @subpackage Tps_Newsletter/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Newsletter {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Tps_Newsletter_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->define_constants();

		$this->plugin_name = 'tps-newsletter';
		$this->version = '2.0.0';

		$this->load_config();
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		
	}

	/**
	 * Define Constants.
	 */
	private function define_constants() {
		
		$this->define( 'TPS_NEWSLETTER_PLUGIN_PATH', WP_PLUGIN_DIR.'/tps-newsletter' );
		$this->define( 'TPS_NEWSLETTER_TEMPLATES_PATH', WP_PLUGIN_DIR.'/tps-newsletter/public/templates' );
		$this->define( 'TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH', WP_PLUGIN_DIR.'/tps-newsletter/public/templates/emails' );
		$this->define( 'TPS_NEWSLETTER_VERSION', $this->version );

	}

	/**
	 * Define constant if not already set.
	 *
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Load config file
	 */
	private function load_config() {

		if ( is_file( TPS_NEWSLETTER_PLUGIN_PATH . '/config.php' ) )
		
			require_once TPS_NEWSLETTER_PLUGIN_PATH . '/config.php';

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Tps_Newsletter_Loader. Orchestrates the hooks of the plugin.
	 * - Tps_Newsletter_i18n. Defines internationalization functionality.
	 * - Tps_Newsletter_Admin. Defines all hooks for the admin area.
	 * - Tps_Newsletter_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for laod Elastic Email PHP wrapper class
		 */
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/ElasticEmailClient.php';

		/**
		 * The class responsible for newsletter contact management
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-ee.php';

		/**
		 * The class responsible for the behavior of native WP emails
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-wp-emails.php';

				/**
		 * The class responsible for newsletter helpers
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-helpers.php';

				/**
		 * The class responsible for newsletter links manipulation
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-links.php';


		/**
		 * The class responsible for newsletter contact management
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-contact.php';

		/**
		* The class responsible for adding newsletter user meta fields
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/user-meta/class-tps-user-meta-newsletter.php';

		/**
		 * The class responsible for email tempalte hooks
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-template-hooks.php';

		/**
		 * The class responsible for registering WC My Account endpoints
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-my-account-endpoints.php';

		/**
		 * The class responsible for registering Emails endpoints
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-emails-endpoints.php';

		/**
		 * The class responsible for showing admin notices
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-admin-notices.php';

		/**
		 * The class responsible for ajax requests
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-ajax.php';

		/**
		 * The class responsible for Contact Not Found frontend notice
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-contact-not-found-notice.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-newsletter-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-tps-newsletter-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-tps-newsletter-public.php';

		$this->loader = new Tps_Newsletter_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Tps_Newsletter_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Tps_Newsletter_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Tps_Newsletter_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Tps_Newsletter_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		
		//Add WC My Account endpoints
		add_action( 'init', array( 'Tps_Newsletter_My_Account_Endpoints', 'rewrite' ) );
 		add_filter( 'query_vars', array( 'Tps_Newsletter_My_Account_Endpoints', 'query_vars' ) );
  		add_action( 'woocommerce_account_subscription_endpoint', array( 'Tps_Newsletter_My_Account_Endpoints', 'woocommerce_account_newsletter_content' ) );
  		add_filter( 'the_title', array( 'Tps_Newsletter_My_Account_Endpoints', 'woocommerce_account_newsletter_endpoint_title' ) );

		//Add Emails endpoints
		$this->loader->add_action( 'init', 'Tps_Newsletter_Emails_Endpoints' , 'rewrite' ) ;
		$this->loader->add_filter( 'query_vars', 'Tps_Newsletter_Emails_Endpoints', 'query_vars' ) ;
		$this->loader->add_filter( 'template_include', 'Tps_Newsletter_Emails_Endpoints', 'template_include' , 15 ) ;
		$this->loader->add_filter( 'wp_title', 'Tps_Newsletter_Emails_Endpoints', 'wp_title' , 1232 ) ;
		
		//Prevent email endpoints from index
		$this->loader->add_action( 'template_redirect', 'Tps_Newsletter_Emails_Endpoints', 'no_index' );
		
  		//Add endpoint support for product-category taxonomy
  		add_filter( 'woocommerce_taxonomy_args_product_cat', array( 'Tps_Newsletter_Emails_Endpoints', 'woocommerce_taxonomy_args_product_cat' ) );

  		//Email template hooks
  		add_action( 'tps-newletter-email-html-start', array( 'Tps_Newsletter_Template_Hooks', 'html_start' ) );
  		add_action( 'tps-newletter-email-html-end', array( 'Tps_Newsletter_Template_Hooks', 'html_end' ) );

		//Show and save Newsletter User meta field in admin profile edit screen
		$this->loader->add_action( 'show_user_profile', 'Tps_User_Meta_Newsletter' , 'render' ) ;
		$this->loader->add_action( 'edit_user_profile', 'Tps_User_Meta_Newsletter' , 'render' ) ;
		$this->loader->add_action( 'personal_options_update', 'Tps_User_Meta_Newsletter', 'save' );
		$this->loader->add_action( 'edit_user_profile_update', 'Tps_User_Meta_Newsletter', 'save' );

		/*
		* Mailing list contact actions
		*/

		//After user registration, add contact to mailing list
		$this->loader->add_action( 'user_register', 'Tps_Newsletter_Contact', 'add' );		
		//Before user delete, delete contact from mailing list
		$this->loader->add_action( 'delete_user', 'Tps_Newsletter_Contact', 'delete' );
		//After user update in backend, update contact in mailing list
		$this->loader->add_action( 'profile_update', 'Tps_Newsletter_Contact', 'profile_update' , 999 , 2 );

		//Before save account details in profile edit in WC frontend, update contact data in mailing list and catch errors if any
		$this->loader->add_action( 'woocommerce_save_account_details_errors', 'Tps_Newsletter_Contact', 'save_account_details' , 10 , 2);

		// After user changes role, update role in mailing list is_customer custom field
		$this->loader->add_action( 'add_user_role', 'Tps_Newsletter_Contact', 'update_field_is_customer_true' , 10 , 2);
		//$this->loader->add_action( 'remove_user_role', 'Tps_Newsletter_Contact', 'update_field_is_customer_false' , 10 , 2);

		// Update db after login 
		$this->loader->add_action( 'set_logged_in_cookie', 'Tps_Newsletter_Contact', 'after_login_update_db' , 10, 5 );
		
		// Update EE after login 
		$this->loader->add_action( 'set_logged_in_cookie', 'Tps_Newsletter_Contact', 'after_login_update_ee' , 20, 5 );
		
		/*
		*********************
		*/		
		
		//Add newsletter menu item in WC MY Account page, via the available filter
		$this->loader->add_filter( 'woocommerce_account_menu_items', $plugin_public, 'add_woocommerce_newsletter_account_menu_item', 10, 1 ) ;

		//Native WP emails
		// Disable email change email notification
		$this->loader->add_filter( 'send_email_change_email', 'Tps_Newsletter_Wp_Emails', 'disable_email_change_email' ) ;

		//Ajax requests
		//Add contact to EE via AJAX
		$this->loader->add_action( 'wp_ajax_tps_newsletter_subscribe', 'Tps_Newsletter_Ajax', 'subscribe' );

		$this->loader->add_action( 'admin_notices', 'Tps_Newsletter_Admin_Notices', 'display' , 999 );	

		/*
		* Show Contact Not Found notice in footer
		*/
		$this->loader->add_action( 'wp_footer', 'Tps_Newsletter_Contact_Not_found_Notice', 'html' );	

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Tps_Newsletter_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
