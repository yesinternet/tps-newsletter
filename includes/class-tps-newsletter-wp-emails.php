<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Newsletter_Wp_Emails class
 * This handles the behavior of WP native emails
 */
class Tps_Newsletter_Wp_Emails {	

	public static function disable_email_change_email()
	{
		return false;	
	}
}