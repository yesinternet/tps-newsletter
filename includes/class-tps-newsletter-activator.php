<?php

/**
 * Fired during plugin activation
 *
 * @link       http://theparentsshop.com/
 * @since      1.0.0
 *
 * @package    Tps_Newsletter
 * @subpackage Tps_Newsletter/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tps_Newsletter
 * @subpackage Tps_Newsletter/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Newsletter_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
