<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Tps_Newsletter_Ajax {

	public static function subscribe(){

	    $user = wp_get_current_user();
	    
	    $subscribe_contact = Tps_Newsletter_Contact::add( $user->ID );
	    
		wp_send_json( $subscribe_contact );  

		wp_die();

	} 
}