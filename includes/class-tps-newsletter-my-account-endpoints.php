<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Newsletter_My_Account_Endpoints class
 *
 */
class Tps_Newsletter_My_Account_Endpoints {

	protected static $endpoints= array(
		TPS_NEWSLETTER_MY_ACCOUNT_ENDPOINT
	);

	/**
	 * Rewrite rules.
	 */
	public static function rewrite() {
		
		global $wp;

		foreach ( self::$endpoints as $key => $endpoint ) {
			add_rewrite_endpoint( $endpoint, EP_ROOT | EP_PAGES );
		}
	}

	/**
	 * Query vars
	 */
	public static function query_vars( $vars ) {

		foreach ( self::$endpoints as $key => $endpoint ) {
			$vars[] = $endpoint;
		}

		return $vars;
	}

	public static function woocommerce_account_newsletter_content( ) {

		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/myaccount/my-subscription.php';
	 
	}

	//Newsletter endpoint title
	public static function woocommerce_account_newsletter_endpoint_title( $title ) {
		
		global $wp_query;

	    $is_endpoint = isset( $wp_query->query_vars[TPS_NEWSLETTER_MY_ACCOUNT_ENDPOINT] );

	    if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {

	        $title = __( 'Email Subscription', 'tps-newslettere' );

	        remove_filter( 'the_title', 'woocommerce_account_newsletter_endpoint_title' );
	    }

   		return $title;
	}

}