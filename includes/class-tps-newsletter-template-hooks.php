<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Newsletter_Template_Hooks class
 *
 */
class Tps_Newsletter_Template_Hooks {

	/**
	 * Email HTML start
	 */
	public static function html_start() {
		
		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/layouts/default-start.php'; 

		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/partials/header.php';
		
		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/partials/container-start.php';
		
		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/partials/spacer-32.php';

	}

	/**
	 * Email HTML start
	 */
	public static function html_end() {

		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/partials/spacer-32.php';

		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/partials/container-end.php';
		
		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/partials/footer.php';

		include TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/layouts/default-end.php'; 

	}

}