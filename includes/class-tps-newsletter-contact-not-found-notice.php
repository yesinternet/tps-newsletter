<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Tps_Newsletter_Contact_Not_Found_Notice {

	public static function html(){

		$current_user = wp_get_current_user();

		if ( !$current_user->exists() ){
			return;
		}

		$_tps_newsletter_not_found = get_user_meta( $current_user -> ID , '_tps_newsletter_not_found');

		if ( !$_tps_newsletter_not_found ){
			return;
		}

		echo '<div class="content-area">';
		echo '<div class="container">';
	
		include TPS_NEWSLETTER_TEMPLATES_PATH . '/contact-not-found-notice.php';
		
		echo '</div>';
		echo '</div>';

	} 
}