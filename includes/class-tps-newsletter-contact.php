<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Newsletter_Contact class
 */
class Tps_Newsletter_Contact {	

	/**
	 * Delete contact to mailing list
	 */
	public static function add( $user_id , $user = null ){

		if ( !$user ) {
			$user = get_userdata( $user_id );
		}

		$contact_data = array(
			//Core fields
			'firstName' => ( isset ( $user -> first_name ) ) ?  $user -> first_name : null ,
			'lastName' => ( isset ( $user -> last_name ) ) ?  $user -> last_name : null ,
			'country' => self::get_user_country(),
			// Custom fields
			'field_gender' => self::get_user_gender( $user -> ID ),
			'field_login_source' => self::get_user_login_source( $user -> ID ),
			'field_last_login' => self::get_user_last_login( $user -> ID ),
			'field_wishlist_updated' => Tps_Wishlist_Helpers::get_user_wishlist_updated_time( $user -> ID ), // TO DO: Add this filed via filter in tps-wishlist plugin
			'field_is_customer' => self::user_is_customer( $user -> ID ),
		);

		$contact_add = Tps_Newsletter_EE_API::contact_add( $user -> user_email , $contact_data );

		if ( $contact_add ['success'] == true ){
			
			delete_user_meta( $user_id , '_tps_newsletter_not_found');
		}
		else{

			Tps_Newsletter_Admin_Notices::add('ee_contact_add_error', $contact_add ['message'] );

			update_user_meta( $user_id , '_tps_newsletter_not_found' , true );		
		}

		return $contact_add;
	}

	/**
	 * Delete contact from mailing list
	 */
	public static function delete( $user_id ){

		$user = get_userdata( $user_id );

		$contact_delete = Tps_Newsletter_EE_API::contact_delete( $user -> user_email );

		if ( $contact_delete ['success'] == false ){
			
			Tps_Newsletter_Admin_Notices::add('ee_contact_delete_error', $contact_delete ['message'] );
			Tps_Newsletter_Admin_Notices::add('ee_contact_delete_error_data', $contact_delete ['data'] );

			wp_safe_redirect( $_POST['_wp_http_referer'] );

			exit();
		}

	}

	/**
	 * Save account details via WooCommerce frontend profile edit screen
	 */
	public static function save_account_details( $errors, $user ) {

		/*
		 * Do not continue if there is already any error, i.e. when passowrds do not match
		 */
		if ( wc_notice_count( 'error' ) !== 0 ) {
			return;
		}

		/*
		 * Check if user has changed their email.
		 */
		$old_user_data = get_userdata( $user -> ID );

		if ( $old_user_data -> user_email != $user -> user_email ){

			// Assign 'Unsubscribe' status to old email
			$contact_changestatus = Tps_Newsletter_EE_API::contact_changestatus( $old_user_data -> user_email , 'Unsubscribed' );
			
			if ( $contact_changestatus ['success'] == false ){

				$errors -> add( 'ee_contact_changestatus_error' , __('Your Profile has not been updated. An error occured when trying to remove your previous email address. Please try again.' , 'tps-newsletter') );

				return;
			}

			$contact_add = self::add( $user -> ID , $user );
			
			if ( $contact_add ['success'] == false ){
				
				$errors -> add( 'ee_contact_add_error' , __('Your Profile has not been updated. An error occured when trying to add your new email address. Please try again.' , 'tps-newsletter') );

				return;
			}		

			return;
		}

		$contact_data = array(
			'field_firstName' => $user -> first_name,
			'field_lastName' => $user -> last_name,
			'field_gender' => self::get_user_gender( $user -> ID ),
		);

		$contact_update = Tps_Newsletter_EE_API::contact_update( $user -> user_email , $contact_data );

		if ( $contact_update ['success'] == false ){
			
			$errors -> add( 'ee_contact_update_error' , __('Your Profile has not been updated. An error occured when trying to update your mailing list data. Please try again.' , 'tps-newsletter') );

			return;
		}
	}

	/**
	 * Backend profile update
	 */
	public static function profile_update( $user_id, $old_user_data ) {

		if( !is_admin() ){
        	return;
        }

		$user = get_userdata( $user_id );

		if ( $old_user_data -> user_email != $user -> user_email ){

			$contact_changestatus = Tps_Newsletter_EE_API::contact_changestatus( $old_user_data -> user_email , 'Unsubscribed' );
			
			if ( $contact_changestatus ['success'] == false ){
				Tps_Newsletter_Admin_Notices::add('ee_contact_changestatus_error', $contact_changestatus ['message'] );
				return;
			}

			$contact_add = self::add( $user_id );
			
			if ( $contact_add ['success'] == false ){
				Tps_Newsletter_Admin_Notices::add('ee_contact_add_error', $contact_add ['message'] );
				return;
			}		


			return;
		}

		$contact_data = array(
				'field_firstName' => $user -> first_name,
				'field_lastName' => $user -> last_name,
				'field_gender' => self::get_user_gender( $user -> ID ),
				'field_is_customer' => self::user_is_customer( $user -> ID ),
		);

		$profile_update = Tps_Newsletter_EE_API::contact_update( $user -> user_email , $contact_data );

		if ( $profile_update['success'] == false ){
			Tps_Newsletter_Admin_Notices::add('ee_contact_update_error', $profile_update['message'] );
		}

		return;

	}

	/**
	 * Update custom field is_customer with value True
	 */
	public static function update_field_is_customer_true( $user_id , $role ) {

		if ( $role != 'customer' ) return;
		
		$contact_data = array(
			'field_is_customer' => true
		);

		$user = get_userdata( $user_id );
		
		Tps_Newsletter_EE_API::contact_update( $user -> user_email , $contact_data );
	}

	/**
	 * Update custom field is_customer with value False
	 */
	/*
	public static function update_field_is_customer_false( $user_id , $role ) {

		if ( $role != 'customer' ) return;

		$contact_data = array(
			'field_is_customer' => false
		);

		$user = get_userdata( $user_id );

		Tps_Newsletter_EE_API::contact_update( $user -> user_email , $contact_data );
	}
	*/

	/**
	 * Update db after user login
	 */
	public static function after_login_update_db( $logged_in_cookie, $expire, $expiration, $user_id, $scheme ) {
		
		$user = get_userdata( $user_id );

		//Save last login timestamp in user meta
		update_user_meta( $user_id, '_tps_last_login', time() );

		/*
		* Social Login plugin
		* The Social login plugin does not update user meta, if the user previously logged in via Wordpress
		*/
		
		//Check if social login plugin has saved related user meta
		$deuid = get_user_meta( $user_id, 'deuid', true );

		if ( empty ($deuid) ){//Empty deuid means that user has registered via WP before

			global $wpdb;
            
            $sql = "SELECT *  FROM  `{$wpdb->prefix}apsl_users_social_profile_details` WHERE  `user_id` =   '$user_id'";

            // Collect the user data from the Social login plugin custom db table
            $row = $wpdb->get_row($sql);

            if( $row ) {

            	//Update user meta
	            update_user_meta( $user_id, 'deuid', $row->identifier );
	            update_user_meta( $user_id, 'first_name', $row->first_name );
	            update_user_meta( $user_id, 'last_name', $row->last_name );
	            update_user_meta( $user_id, 'billing_first_name', $row->first_name );
	            update_user_meta( $user_id, 'billing_last_name', $row->last_name );
	            update_user_meta( $user_id, 'deutype', $row->provider_name );
	            update_user_meta( $user_id, 'deuimage', $row->photo_url );
	            update_user_meta( $user_id, 'sex', $row->gender );
	            update_user_meta( $user_id, 'display_name', $row->first_name . ' ' . $row->last_name );
	            update_user_meta( $user_id, 'user_url', $row->profile_url );

            }

		}

	}
	
	/**
	 * Update EE after user login
	 */
	public static function after_login_update_ee( $logged_in_cookie, $expire, $expiration, $user_id, $scheme ) {
		
		$user = get_userdata( $user_id );
		
		$contact_data = array(
				'field_firstName' => get_user_meta( $user -> ID, 'first_name', true ),
				'field_lastName' => get_user_meta( $user -> ID, 'last_name', true ),
				'field_login_source' => self::get_user_login_source( $user -> ID ),
				'field_last_login' => self::get_user_last_login( $user -> ID ),
				'field_gender' => self::get_user_gender( $user -> ID ),
		);

		$contact_update = Tps_Newsletter_EE_API::contact_update( $user -> user_email , $contact_data );

	}

	/*
	 * Helper functions
	 */

	private static function get_user_country(){

		$location =  WC_Geolocation::geolocate_ip();
		$country_code =  $location['country'];
		$country = WC()->countries->countries[$country_code];	

		return $country;
	}

	private static function get_user_gender( $user_id ){

		if ( isset ( $_POST['sex'] ) ){
			$sex = ( !empty ( $_POST['sex'] ) ) ? sanitize_text_field ( $_POST['sex'] ) : null;
		}
		else{
			$sex = get_user_meta( $user_id , 'sex', true );
		}

		$gender = ( !empty( $sex ) ) ? $sex[0] : null ;
		
		return $gender; 
	}

	private static function get_user_last_login( $user_id ){

		$last_login = get_user_meta( $user_id , '_tps_last_login', true );
		
		if ( empty( $last_login ) ){

			update_user_meta( $user_id, '_tps_last_login', $last_login = time() );
		}

		return Tps_Newsletter_EE_API::get_formatted_timestamp( $last_login ) ;
	}

	private static function get_user_login_source( $user_id ){

		$login_source = get_user_meta( $user_id , 'deutype', true );

		if ( empty( $login_source ) ){

			update_user_meta( $user_id, 'deutype', $login_source = 'wordpress' );
		}

		return $login_source;
	}

	private static function user_is_customer( $user_id ){

		$isCustomer = false;

		$user = new WP_User( $user_id );

		foreach( $user->roles as $role ) {
	    	
	    	if ( $role == 'customer' ) { $isCustomer = true; }
	    }

	    return $isCustomer;
	}
}