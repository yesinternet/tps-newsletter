<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Newsletter_Helpers class
 */
class Tps_Newsletter_Helpers {


	protected static $root_endpoint = TPS_NEWSLETTER_EMAILS_ENDPOINT ;
	
	protected static $permalinks_endpoint = TPS_NEWSLETTER_EMAILS_PERMALINKS_ENDPOINT ;

	protected static $emails_endpoints = array(
		TPS_NEWSLETTER_EMAILS_WELCOME_ENDPOINT,
		TPS_NEWSLETTER_EMAILS_FEATURED_ENDPOINT,
		TPS_NEWSLETTER_EMAILS_ONSALE_ENDPOINT
	);
	
	/*
	* Check id we are in an email endpoint 
	*/
	public static function is_email_endpoint() {

		global $wp_query;

		foreach ( self::gat_all_email_endpoints() as $key => $endpoint)
		{
			if ( isset( $wp_query->query_vars[ $endpoint ] ) )
			{
				return true;
			}
		}

		return false;

	}

	/*
	* Return all possible email endpoints, including emails endpoints (see below) and permalinks endpoint 
	*/

	private static function gat_all_email_endpoints()
	{
		$endpoints = array();
		
		array_push ( $endpoints , self::$root_endpoint ) ;
		array_push ( $endpoints , self::$permalinks_endpoint ) ;

		foreach ( self::get_emails_endpoints() as $key => $endpoint )
		{
			array_push ( $endpoints , $endpoint ) ;
		}

		return $endpoints;
	}

	/*
	* Return all endpoints that are supported after /emails route. 
	* I.e. /emails/welcome, /emails/onsale etc 
	*/

	public static function get_emails_endpoints(){

		$emails_endpoints = apply_filters( 'tps_newsletter_emails_endpoints', self::$emails_endpoints );
		
		return $emails_endpoints;
	}

}