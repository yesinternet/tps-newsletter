<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
* Tps_Newsletter_Emails_Endpoints class
*/

class Tps_Newsletter_Emails_Endpoints {

	protected static $root_endpoint = TPS_NEWSLETTER_EMAILS_ENDPOINT ;
	
	protected static $permalinks_endpoint = TPS_NEWSLETTER_EMAILS_PERMALINKS_ENDPOINT ;

	/**
	 * Rewrite rules.
	 */
	public static function rewrite() {

		$root_endpoint = self::$root_endpoint;
		
		$emails_endpoints = Tps_Newsletter_Helpers::get_emails_endpoints();

		foreach ( $emails_endpoints as $key => $endpoint ) 
		{
			add_rewrite_rule( '^'.$root_endpoint.'/'.$endpoint.'$', 'index.php?'.$endpoint.'=1', 'top' );
		}

		$permalinks_endpoint = self::$permalinks_endpoint;

		if ( ! isset ( $_POST[$permalinks_endpoint] ) )
		{
			add_rewrite_endpoint( $permalinks_endpoint, EP_CATEGORIES | EP_PERMALINK);
		}

	}

	/**
	 * Query vars
	 */
	public static function query_vars( $vars ) {

		$endpoints = Tps_Newsletter_Helpers::get_emails_endpoints() ; 

		$endpoints[] = self::$permalinks_endpoint;

		foreach ( $endpoints as $key => $endpoint ) 
		{
			if ( ! isset ( $_POST[ $endpoint ] ) )
			{
				$vars[] = $endpoint;
			}
		}

		return $vars;
	}

	public static function template_include( $template ) {

		$endpoints = Tps_Newsletter_Helpers::get_emails_endpoints() ; 

		$endpoints[] = self::$permalinks_endpoint;

		foreach ( $endpoints as $key => $endpoint ) 
		{
	 		if( get_query_var( $endpoint, false ) !== false ) {
	 
				//Check theme directory first
				$newTemplate = locate_template( array( 'tps-newsletter/emails/endpoints/'.$endpoint.'.php' ) );
				
				if( '' != $newTemplate ){	
					return $newTemplate;
		 		}
				
				//Check plugin directory next
				$newTemplate = TPS_NEWSLETTER_PLUGIN_PATH . '/public/templates/emails/endpoints/'.$endpoint.'.php';

				if( file_exists( $newTemplate ) ){	
					return $newTemplate;
				}

			}

		}

		return apply_filters( 'tps_newsletter_email_template', $template );

	}

	//Email endpoints titles
	public static function wp_title( $title ) {
		
		global $wp_query;

	    if ( isset ( $wp_query->query_vars[TPS_NEWSLETTER_EMAILS_WELCOME_ENDPOINT] ) )
	    {
	    	$title='Welcome to our Community';
	    }

	    if ( isset ( $wp_query->query_vars[TPS_NEWSLETTER_EMAILS_FEATURED_ENDPOINT] ) )
	    {
	    	$title='Recommended for you by the Parents Shop';
	    }

	    if ( isset ( $wp_query->query_vars[TPS_NEWSLETTER_EMAILS_ONSALE_ENDPOINT] ) )
	    {
	    	$title='On Sale by the Parents Shop';
	    }

   		return apply_filters( 'tps_newsletter_email_title', $title );

	}

	public static function woocommerce_taxonomy_args_product_cat( $args ) {
		
		$args['rewrite']['ep_mask'] = EP_CATEGORIES;

		return $args;

	}

	/**
	 * No index our email endpoints.
	 * Prevent indexing pages like /emails/welcome.
	 *
	 */
	public static function no_index(){

		if ( Tps_Newsletter_Helpers::is_email_endpoint() ) {

			@header( 'X-Robots-Tag: noindex' );
		}
	}

}