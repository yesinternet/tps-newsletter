<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Newsletter_Admin_Notices class
 */
class Tps_Newsletter_Admin_Notices {	

	public static function add( $code, $message, $data = '' ){

		if ( !is_admin() ) return;

		$tps_newsletter_admin_notices = get_transient( 'tps_newsletter_admin_notices' );

		if ( !is_wp_error( $tps_newsletter_admin_notices ) ){
			$tps_newsletter_admin_notices = new WP_Error();
		}
		
		$tps_newsletter_admin_notices -> add ( $code, $message, $data );
		
		set_transient("tps_newsletter_admin_notices", $tps_newsletter_admin_notices, 45);
	
	}

	public static function display(){
	
		$tps_newsletter_admin_notices = get_transient( "tps_newsletter_admin_notices" );
		
		if ( is_wp_error( $tps_newsletter_admin_notices ) ) { 

			foreach ( $tps_newsletter_admin_notices -> get_error_messages () as $error){

			?>

			  <div class="tps_newsletter_admin_notice error">
			      <p><?php echo $error; ?></p>
			  </div>
			
			<?php
				
			}

			delete_transient("tps_newsletter_admin_notices");

		}

	}
}