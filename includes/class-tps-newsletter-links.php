<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Newsletter_Links class
 */
class Tps_Newsletter_Links {

	public static function utm( $url , $utm ) {

		$campaign_params = array();

		$campaign_params['utm_source'] = "theparentsshop"; // Required
		$campaign_params['utm_campaign'] = wp_title('',false);
		$campaign_params['utm_medium'] = "email";

		$campaign_params = array_merge( $campaign_params , $utm );
		
		return $url . '?' . http_build_query($campaign_params);
	}

}