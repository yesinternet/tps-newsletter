<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://theparentsshop.com/
 * @since             1.0.0
 * @package           Tps_Newsletter
 *
 * @wordpress-plugin
 * Plugin Name:       The Parents Shop Newsletter
 * Plugin URI:        http://theparentsshop.com/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           2.0.0
 * Author:            Haris Kaklamanos
 * Author URI:        http://theparentsshop.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tps-newsletter
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tps-newsletter-activator.php
 */
function activate_tps_newsletter() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tps-newsletter-activator.php';
	Tps_Newsletter_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tps-newsletter-deactivator.php
 */
function deactivate_tps_newsletter() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tps-newsletter-deactivator.php';
	Tps_Newsletter_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_tps_newsletter' );
register_deactivation_hook( __FILE__, 'deactivate_tps_newsletter' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tps-newsletter.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_tps_newsletter() {

	$plugin = new Tps_Newsletter();
	$plugin->run();

}
run_tps_newsletter();

function tps_utm( $url , $utm)
{
	return Tps_Newsletter_Links::utm ( $url , $utm ) ;
}
