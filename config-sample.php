<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'TPS_NEWSLETTER_MY_ACCOUNT_ENDPOINT', 'subscription' );

define( 'TPS_NEWSLETTER_EMAILS_PERMALINKS_ENDPOINT', 'email' );
define( 'TPS_NEWSLETTER_EMAILS_ENDPOINT', 'emails' );

define( 'TPS_NEWSLETTER_EMAILS_WELCOME_ENDPOINT', 'welcome' );
define( 'TPS_NEWSLETTER_EMAILS_FEATURED_ENDPOINT', 'recommended' );
define( 'TPS_NEWSLETTER_EMAILS_ONSALE_ENDPOINT', 'onsale' );

define( 'TPS_NEWSLETTER_EMAILS_LOOP_PRODUCTS_NO', 6 );

define( 'TPS_NEWSLETTER_ELASTICEMAIL_API_KEY', 'xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxx' );
define( 'TPS_NEWSLETTER_ELASTICEMAIL_PUBLIC_ACCOUNT_ID', 'xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxx' );


define( 'TPS_NEWSLETTER_COUNTDOWN_URL', 'http://countdown.theparentsshop.com/gif.php?time=' );