<?php
/**
 * Newsletter User Meta
 *
 * Displays the newsletter subscription profile edit fields
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_User_Meta_Newsletter Class.
 */
class Tps_User_Meta_Newsletter {

   /**
	* The HTML for the user meta fields
	*/
	public static function render( $user ) {

		$email = $user -> data -> user_email;	

		$contact = Tps_Newsletter_EE_API::contact_load( $email );

		$_tps_newsletter_not_found = get_user_meta( $user -> ID , '_tps_newsletter_not_found' , true );

		?>

		<h3><?php _e( 'Email Subscription' , 'tps-dashboard' );?></h3>

		<?php 

		if ( $_tps_newsletter_not_found ){
		?>
		
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row"><?php _e('Status','tps-dashboard');?></th>
					<td>
						<fieldset>
							<legend class="screen-reader-text"><span><?php _e('Email Subscription Status','tps-dashboard');?></span></legend>
								<label for="_tps_newsletter_add_contact">
									<input name="_tps_newsletter_add_contact" id="_tps_newsletter_add_contact" value="yes" type="checkbox">
									<?php _e('Check to add user to the mailing list contacts','tps-dashboard');?>						
								</label>
								<p class="description"><?php _e('The user was never added to the mailing list probably due to an error.','tps-dashboard');?></p>
						</fieldset>
					</td>
				</tr>
			</tbody>
		</table>


		<div class="error notice">
            <p>
                <?php _e('It seems that this user was never added to the mailing list probably due to an error. You can add the user below.','tps-dashboard');?>
            </p>
        </div>


		<?php 
		}


		if ( $contact ['success'] === true )
		{
			add_thickbox();

			$contact_source_map = Tps_Newsletter_EE_API::contact_status_map();
			$contact_source = $contact_source_map [  $contact ['data'] -> source] ;
			
			$contact_status_map = Tps_Newsletter_EE_API::contact_status_map();
			$contact_status = $contact_status_map [  $contact ['data'] -> status] ;			
		}

		 if ( $contact ['success'] === true ) :?> 

			

			<table class="form-table">

				<tr>
					<th><label for="tps-newsletter-active"><?php _e( 'Contact details' , 'tps-dashboard' );?></label></th>

					<td>
						
						<a href="#TB_inline?width=600&height=550&inlineId=modal-contact-details" title="<?php _e( 'Contact details' , 'tps-dashboard' );?>" class="thickbox"><?php _e( 'Click to see contact email preferences details' , 'tps-dashboard' );?></a>
						
						<div id="modal-contact-details" style="display:none;">
							<p>
		    				
		    				<?php foreach ( $contact ['data'] as $property => $value ) :?>
		    					
		    					<strong><?php echo "$property";?>:</strong>&nbsp;<?php echo "$value";?><br/>
		    				
		    				<?php endforeach;?>

		    				</p>

						</div>
					</td>
				
				</tr>

				<tr>
					<th><label for="tps-newsletter-dateadded"><?php _e( 'Date added' , 'tps-dashboard' );?></label></th>

					<td>
						 <?php echo  $contact ['data'] -> dateadded ;?>
					</td>
				</tr>

				<tr>
					<th><label for="tps-newsletter-dateupdated"><?php _e( 'Date updated' , 'tps-dashboard' );?></label></th>

					<td>
						 <?php echo  $contact ['data'] -> dateupdated ;?>
					</td>
				</tr>

				<tr>
					<th><label for="tps-newsletter-country"><?php _e( 'Country' , 'tps-dashboard' );?></label></th>

					<td>
						 <?php echo  $contact ['data'] -> country ;?>
					</td>
				</tr>

				<tr>
					<th><label for="tps-newsletter-source"><?php _e( 'Source' , 'tps-dashboard' );?></label></th>

					<td>
						<?php echo $contact_source ;?>
					</td>

				</tr>

				<tr>
					<th><label for="tps-newsletter-status"><?php _e( 'Status' , 'tps-dashboard' );?></label></th>

					<td>				
						<?php echo $contact_status ;?>
					</td>

				</tr>

			</table>
		
		<?php endif ;?>

		<?php 

	}


	public static function save( $user_id  ) {
		
		if ( !current_user_can( 'edit_user', $user_id ) )
			return false;

		if ( isset( $_POST['_tps_newsletter_add_contact'] ) && $_POST['_tps_newsletter_add_contact'] == 'yes' )
		{
			Tps_Newsletter_Contact::add($user_id);
		}
	}

}