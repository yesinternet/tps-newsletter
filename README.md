#How to setup the plugin
Create a config.php file inside the plugin root folder, based on the config.sample.php file. Fill in the plugin configuration settings inside config.php.

#Email endpoints

* https://www.theparentsshop.com/emails/welcome
* https://www.theparentsshop.com/emails/onsale
* https://www.theparentsshop.com/emails/recommended

* https://www.theparentsshop.com/category/[category_name]/email
* https://www.theparentsshop.com/product/[product_name]/email
* https://www.theparentsshop.com/product/[product_name]/email

* https://www.theparentsshop.com/emails/user_wishlist/?user_email=[user_email]

#How to setup Elastic Email

## API Keys
Copy the API Key & Public Account ID from the settings page, in order to insert them inside the config.php file.

##Core fields
* firstName
* LastName
* country

## Custom fields

Create the following custom fields:

* gender (string,1)
* login_source (string,20)
* last_login (datetime)
* is_customer (boolean)
* wishlist_updated (datetime)

## Campaigns
Create the following campaigns:

### Wishlist reminder - 1 day
The 1 day Wishlist reminder campaign will be sent to subscibers that have at least 1 product in their wishlist, 1 day after the last time they added (or removed) a product in their wishlist.

####List
Create a list inside Contacts menu with the following settings
* Name: Wishlist Reminder - 1 day
* Query: daysfromwishlist_updated >= 1 AND daysfromwishlist_updated < 2

####Campaign
Create a Campaign with the following settings
* To: The list mentioned above
* Choose the template
* Schedule: Every day at 11:00, Send indefinately every 1 Days

####Template Content
```
{{
var html = download('http://demo.theparentsshop.com/emails/user_wishlist/?user_email='+ encodeURIComponent(email) );
var subject = htmltag(html, 'title');
var body = htmltag(html, 'body');
}}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>{{subject}}
    </title>
    </style>
  </head>
{{body}}
</html>
```